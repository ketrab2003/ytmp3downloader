import youtube_dl
import sys
import subprocess
import os

if len(sys.argv) < 2:
    vids_url = input('Video/playlist url: ')
else:
    vids_url = sys.argv[1]
vids_dir = input('Specify download directory (press ENTER for active directory): ')
vids_num = int(input('Number of videos to download: '))

ydl_opts = {
    'outtmpl': os.path.join(vids_dir, '%(title)s.%(ext)s'),
    'format': 'mp4',
}
if vids_num > 0:
    ydl_opts['playlistend'] = vids_num

with youtube_dl.YoutubeDL(ydl_opts) as ydl:
    # get playlist info
    info = ydl.extract_info(vids_url, download=False)

    # download playlist
    for vid in info['entries']:
        filepath = ydl.prepare_filename(vid)
        mp3filepath = os.path.splitext(filepath)[0] + '.mp3'

        if os.path.isfile(filepath) \
           or os.path.isfile(mp3filepath):
            print(f"{vid['title']} already downloaded - skipping...")
            continue

        # download video
        print(f"downloading {vid['title']} to {filepath} ...")
        try:
            ydl.download([vid['webpage_url']])
        except Exception as e:
            print(f"some errors occured: {str(e)}")
            continue
            
        # convert video to mp3
        if not os.path.isfile(mp3filepath):
            print(f"converting to {mp3filepath} ...")
            subprocess.run([
                './ffmpeg.exe',
                '-i', filepath,
                mp3filepath
            ])
            # remove video file
            os.remove(filepath)

print("done.")